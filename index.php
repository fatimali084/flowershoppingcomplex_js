<?php


include "header.php";
?>

   <div class="main main-raised">
      
		<div id="hot-deal" class="section mainn mainn-raised">
			
			<div class="container">
				
				<div class="row">
					<div class="col-md-12">
						<div class="hot-deal">
							<h2 class="text-uppercase">hot deal this week</h2>
							<p>New Collection Up to 50% OFF</p>
							<a class="primary-btn cta-btn" href="index.php">Shop now</a>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
		
		<div class="section">
			
			<div class="container">
				
				<div class="row">

					
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">New Products</h3>
						</div>
						<?php include 'db.php';
								
                    
									$product_query = "SELECT * FROM products";
                				     $run_query = mysqli_query($con,$product_query);
                						if(mysqli_num_rows($run_query) > 0){

                  					  while($row = mysqli_fetch_array($run_query)){
						                        $pro_id    = $row['product_id'];
						                        $pro_title = $row['product_title'];
						                        $pro_price = $row['product_price'];
						                        $product_desc = $row["product_desc"];
												$pro_image = $row['product_image'];

 							         echo "<div class='col-md-3'>
											<div class='product'>
												<a href='product.php?p=$pro_id'><div class='product-img'>
													<img src='product_images/$pro_image' style='max-height: 170px;' alt=''>
													
												</div></a>
												<div class='product-body'>
													
													<h3 class='product-name header-cart-item-name'><a href='product.php?p=$pro_id'>$pro_title</a></h3>
													<h4 class='product-price header-cart-item-info'>$pro_price<del class='product-old-price'>$990.00</del></h4>
													
													<div><button pid='$pro_id' id='product' class='add-to-cart-btn block2-btn-towishlist btn' href='#' style='background-color:#D6547A; color:#fff;'><i class='fa fa-shopping-cart' ></i> add to cart</button></div>
												</div>
												</div>
											</div>
			                               
										
                        
			";}
        ;
      
}
?>
						</div>
					
					

					

	
		

	</div>	

</div>
	</div>	
</div>
<?php  
include "footer.php";
?>
		
		