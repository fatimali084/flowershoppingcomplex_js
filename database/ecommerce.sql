-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 01:48 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getcat` (IN `cid` INT)  SELECT * FROM categories WHERE cat_id=cid$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(250) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `p_id`, `ip_add`, `user_id`, `qty`) VALUES
(6, 26, '::1', 4, 1),
(9, 10, '::1', 7, 1),
(10, 11, '::1', 7, 1),
(11, 45, '::1', 7, 1),
(44, 5, '::1', 3, 2),
(46, 2, '::1', 3, 0),
(49, 60, '::1', 8, 1),
(50, 61, '::1', 8, 1),
(51, 1, '::1', 8, 1),
(52, 5, '::1', 9, 2),
(53, 2, '::1', 14, 1),
(54, 3, '::1', 14, 1),
(55, 5, '::1', 14, 2),
(56, 1, '::1', 9, 1),
(57, 2, '::1', 9, 1),
(71, 61, '127.0.0.1', -1, 1),
(147, 70, '::1', 26, 1),
(185, 1, '::1', 0, 3),
(188, 3, '::1', 125, 1),
(190, 2, '::1', 38, 1),
(191, 5, '::1', 30, 1),
(193, 1, '::1', 126, 1),
(194, 2, '::1', 126, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `trx_id` varchar(255) NOT NULL,
  `p_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `product_id`, `qty`, `trx_id`, `p_status`) VALUES
(1, 12, 7, 1, '07M47684BS5725041', 'Completed'),
(2, 14, 2, 1, '07M47684BS5725041', 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `orders_info`
--

CREATE TABLE `orders_info` (
  `order_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` int(10) NOT NULL,
  `cardname` varchar(255) NOT NULL,
  `cardnumber` varchar(20) NOT NULL,
  `expdate` varchar(255) NOT NULL,
  `prod_count` int(15) DEFAULT NULL,
  `total_amt` int(15) DEFAULT NULL,
  `cvv` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_info`
--

INSERT INTO `orders_info` (`order_id`, `user_id`, `f_name`, `email`, `address`, `city`, `state`, `zip`, `cardname`, `cardnumber`, `expdate`, `prod_count`, `total_amt`, `cvv`) VALUES
(1, 12, 'Puneeth', 'puneethreddy951@gmail.com', 'Bangalore, Kumbalagodu, Karnataka', 'Bangalore', 'Karnataka', 560074, 'pokjhgfcxc', '4321 2345 6788 7654', '12/90', 3, 77000, 1234),
(2, 27, 'Vielka', 'lofiqumuqa@mailinator.com', 'Ipsum id facere corr', 'Natus est autem qui ', 'Expedita ut deserunt', 90081, 'Aurelia Lindsey', '680', '12/22', 2, 8500, 123),
(3, 27, 'Mechelle', 'dotoke@mailinator.net', 'Molestiae eum ea rep', 'Laboriosam qui enim', 'Nemo sint ex possimu', 85331, 'Zena West', '10', '12/23', 1, 3500, 123),
(4, 27, 'Katelyn', 'xolynogi@mailinator.com', 'Quo laudantium porr', 'Sit obcaecati minima', 'Consequat Nisi ipsu', 89026, 'Allistair Vaughan', '428', '12/23', 1, 5000, 123),
(5, 27, 'Indira', 'denacomyt@mailinator.com', 'Sed incidunt volupt', 'Ullam consequatur N', 'Qui ad voluptates un', 35101, 'Yasir Mcneil', '630', '07/23', 1, 399, 123),
(6, 27, 'Deacon', 'sesi@mailinator.net', 'Rem commodo ipsum pa', 'Cillum voluptas prae', 'Vel odit officia num', 98696, 'Theodore Blake', '380', '12/23', 1, 700, 124),
(7, 27, 'Uriel', 'fawise@mailinator.net', 'Eum sunt itaque pari', 'Mollitia et sunt sin', 'Vitae voluptate assu', 10910, 'Britanney Hebert', '180', '12/23', 4, 225, 123),
(8, 27, 'Madison', 'cosogagi@mailinator.com', 'Corporis in qui non ', 'Dolore facilis tempo', 'Et voluptatum et ali', 38251, 'Rina Mueller', '165', '12/34', 1, 55, 864),
(9, 27, 'Hop', 'wyratulono@mailinator.net', 'Aut ipsum dolorem ve', 'Ipsum dolore molliti', 'Magna beatae omnis s', 82355, 'Jonas Guerrero', '158', '12/45', 4, 581, 125),
(10, 27, 'Jessica', 'tohadeny@mailinator.com', 'Lorem laboris in hic', 'Maxime dolor molesti', 'In nihil qui eos exe', 43217, 'Shafira Ryan', '312', '12/34', 1, 55, 543),
(11, 27, 'Magee', 'dona@mailinator.net', 'Mollit sit ab molli', 'Ratione laudantium ', 'Libero eveniet quis', 64850, 'Ursa Webb', '170', '12/23', 1, 55, 124),
(12, 27, 'Callum', 'maruhugero@mailinator.net', 'Molestiae maiores po', 'Omnis ea quos recusa', 'Nulla duis voluptas ', 39867, 'Signe Glenn', '332', '12/23', 1, 53, 457),
(13, 27, 'Ulric', 'vukyjur@mailinator.com', 'Ratione nihil atque ', 'Error dolore libero ', 'Unde accusamus cum a', 97923, 'Raphael Cooke', '985', '03/23', 2, 113, 321),
(14, 27, 'Levi', 'tily@mailinator.net', 'Ea voluptate rerum a', 'Id quae fugiat cupid', 'Culpa et beatae veri', 43218, 'Risa Horn', '502', '12/23', 1, 55, 344),
(15, 27, 'Martena', 'tuputy@mailinator.com', 'Amet exercitationem', 'Eaque quae ex quaera', 'Consequuntur numquam', 46653, 'Selma Snyder', '689', '12/32', 1, 55, 123),
(16, 27, 'Cullen', 'hotesemyj@mailinator.net', 'Quidem eius quo dolo', 'Irure odio qui ut ex', 'In magna atque ut ad', 15039, 'Sawyer Patterson', '927', '12/33', 1, 55, 123),
(17, 27, 'Chaney', 'husa@mailinator.net', 'Excepturi quo debiti', 'Minim eum dolor ulla', 'Quibusdam impedit i', 53235, 'Marcia Fletcher', '331', '12/23', 1, 399, 123),
(18, 124, 'Latifah', 'wimo@mailinator.com', 'Sit tempora a vel do', 'Qui necessitatibus t', 'Qui consequatur Aut', 10374, 'Brittany Frye', '280', '12/23', 1, 55, 123);

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `order_pro_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(15) DEFAULT NULL,
  `amt` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`order_pro_id`, `order_id`, `product_id`, `qty`, `amt`) VALUES
(97, 7, 1, 2, 118),
(98, 7, 5, 2, 106),
(99, 7, 2, 1, 58),
(100, 7, 3, 1, 55),
(101, 8, 3, 1, 55),
(102, 9, 1, 1, 59),
(103, 9, 6, 1, 53),
(104, 9, 7, 1, 399),
(105, 9, 8, 4, 280),
(106, 10, 3, 1, 55),
(107, 11, 3, 1, 55),
(108, 12, 6, 1, 53),
(109, 13, 2, 1, 58),
(110, 13, 3, 4, 220),
(111, 14, 3, 3, 165),
(112, 15, 4, 1, 55),
(113, 16, 4, 9, 495),
(114, 17, 7, 1, 399),
(115, 18, 4, 6, 330);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(100) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title`, `product_price`, `product_desc`, `product_image`) VALUES
(1, 'Sweet Garden Hatbox', 59, 'Our Sweet Garden Hatbox, designed by our top Florists, featuring blush pink roses with pink lisianthus and gypsophilia makes the perfect gift.\r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '1.png'),
(2, 'Sweet Garden Basket', 58, 'Our Sweet Garden Basket, arranged by our top Florists and features blush pink roses and tulips with pink lisianthus and gypsophilia to make a lovely traditional gift. \r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '2.png'),
(3, 'Vibrant Hatbox', 55, 'Our signature Hat Box lovingly arranged with Yellow Roses, Chrysanthemum, Lisianthus, Solidago, Gerbera, Veronica and Eucalyptus.  \r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '3.png'),
(4, 'Lavender Floral Hatbox', 55, 'Lavender Floral Hatbox designed by our top Florists, the perfect gift for any occasion, we have chosen the finest fresh flowers imported from the Netherlands.\r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '4.png'),
(5, '2 Stem Pink Orchid Plant', 53, 'The very stylish Pink Phalaenopsis orchid is a contemporary favourite, not only because of its chic, decorative flowers but for its captivating elegance too. This gift duo is a fine example of the Phalaenopsis orchid at its best – an abundance of simply exquisite flowers in all their glory.\r\n\r\nFeaturing a pair of purple duo-stemmed Phalaenopsis orchids planted in a classic vase and makes the perfect gift for that someone special.\r\n\r\nOur flowers are sourced from the finest farms in Ireland, South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.\r\n\r\nWe have the best Customer Service i', '5.png'),
(6, '2 Stem White Phalaenopsis ', 53, 'This elegant 2 stem white Phalaenopsis Orchid is planted in a classic vase and makes the perfect gift for that someone special. Ornate yet understated, this beautiful orchid is perfect for displaying on a mantelpiece or coffee table.\r\n\r\nOur flowers are sourced from the finest farms in Ireland, South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '6.png'),
(7, '100 Purple Rose Heart', 399, '100 of the most exquisite and finest Purple roses are perfectly arranged by one of our artisan florists in one of our heart-shaped boxes have a special feminine charm that they will surely love. Imagine the blissful pleasure of this awakening thought. An ultimate symbol of love and this beautiful bouquet sent to your loved one will reaffirm all the beautiful feelings that both of you share.\r\n\r\nThis is also available in White, Pink or Red. \r\n\r\nOur flowers are sourced from the finest farms in Ireland, South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '7.png'),
(8, 'Dozen Red Deluxe Hatbox', 70, 'Nothing says it more than a hatbox of Roses, At Flowers.ie we will select the finest red roses sure to send hearts spinning ??????. Featuring large-headed and spray red roses with greeneries, Hand created by our expert team and presented in gift packaging.\r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', '8.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) VALUES
(12, 'puneeth', 'Reddy', 'puneethreddy951@gmail.com', 'puneeth', '9448121558', '123456789', 'sdcjns,djc'),
(15, 'hemu', 'ajhgdg', 'puneethreddy951@gmail.com', '346778', '536487276', ',mdnbca', 'asdmhmhvbv'),
(16, 'venky', 'vs', 'venkey@gmail.com', '1234534', '9877654334', 'snhdgvajfehyfygv', 'asdjbhfkeur'),
(19, 'abhishek', 'bs', 'abhishekbs@gmail.com', 'asdcsdcc', '9871236534', 'bangalore', 'hassan'),
(21, 'prajval', 'mcta', 'prajvalmcta@gmail.com', '1234545662', '202-555-01', 'bangalore', 'kumbalagodu'),
(22, 'puneeth', 'v', 'hemu@gmail.com', '1234534', '9877654334', 'snhdgvajfehyfygv', 'asdjbhfkeur'),
(23, 'hemanth', 'reddy', 'hemanth@gmail.com', 'Puneeth@123', '9876543234', 'Bangalore', 'Kumbalagodu'),
(24, 'newuser', 'user', 'newuser@gmail.com', 'puneeth@123', '9535688928', 'Bangalore', 'Kumbalagodu'),
(25, 'otheruser', 'user', 'otheruser@gmail.com', 'puneeth@123', '9535688928', 'Bangalore', 'Kumbalagodu'),
(26, 'Guy Irwin', 'Patience Ochoa', 'qyjyhatyf@mailinator.net', 'Fat128546', '0504344346', '19 East Hague Court', 'Occaecat vo'),
(27, 'Josephine Weber', 'Yvette Tillman', 'zydumi@mailinator.net', 'Pa$$w0rd!', '1234567890', '181 West Green Cowley Drive', 'Non ipsum q'),
(123, '2345', '2345', '2345', '2345', '2345', '23456', '2345'),
(124, 'Edward Chapman', 'Stacy Guzman', 'fatimag@mailinator.net', 'Pa$$w0rd!', '1234567898', '218 South Green Fabien Freeway', 'Libero dolo'),
(125, 'Thane Meyer', 'Prescott Howell', 'jaro@mailinator.net', 'Pa$$w0rd!', '1234567898', '749 North Green Milton Court', 'Excepteur p'),
(126, 'qwer', 'efregtr', 'dsf@gmail.com', '1qaz2wsx3edc', '1234567654', 'sadfgbhn', 'xcsdvfgbh'),
(127, 'qwer', 'efregtr', 'dsf@gmail.com', '1qaz2wsx3edc', '1234567654', 'sadfgbhn', 'xcsdvfgbh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `orders_info`
--
ALTER TABLE `orders_info`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`order_pro_id`),
  ADD KEY `order_products` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders_info`
--
ALTER TABLE `orders_info`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `order_pro_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders_info`
--
ALTER TABLE `orders_info`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`);

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products` FOREIGN KEY (`order_id`) REFERENCES `orders_info` (`order_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
